import java.lang.reflect.*;
import java.util.ArrayList;
public class eson {
    
    private  String Json;
    private int index;
    
    public eson(String Json) {
        this.Json = Json;
        this.index=0;
    }
            
    public eson() {
       this.Json="";
       this.index=0;
    }

    public void setJson(String Json) {
        this.Json = Json;
    }

    public String getJson() {
        return Json;
    }
    private int  parseInt(){
        int number=0;
        while(Json.charAt(index)!=','&&Json.charAt(index)!='}'&&Json.charAt(index)!=']'){
            number=number*10+(Json.charAt(index)-'0');
            index++;
        }
        return number;
    }
    
    private double parsedouble(){
        double number=0;
        while(Json.charAt(index)!=','&&Json.charAt(index)!='}'&&Json.charAt(index)!=']')
        {
            if(Json.charAt(index)=='.'){
                index++;
                int i=1;
                while(Json.charAt(index)!=','&&Json.charAt(index)!='}'&&Json.charAt(index)!=']'){                        
                    number+=((double)(Json.charAt(index)-'0'))/((double)10*i);
                    i*=10;
                    index++;
                }
            }else{
                number=number*10+Json.charAt(index)-'0';
                index++;
            }
            
        }
        return number;
    }
    private String doubleOrInt(int number){
        while(Json.charAt(number)!=','&&Json.charAt(number)!='}'){
            if(Json.charAt(number)=='.'){
                return "double";
            }
            number++;
        }
        return "int";
    }
    private String deleteAdditionalChars(String str){
        String string="";
        for(int i=0;i<str.length();){
            if(str.charAt(i)=='\"'){
                string=string+str.charAt(i);
                i++;
                while(str.charAt(i)!='\"'){
                string=string+str.charAt(i);    
                    i++;
                }
                string=string+str.charAt(i);
               i++;
            }else if(str.charAt(i)==' '){
                i++;
            }else{
                string=string+str.charAt(i);
                i++;
            }
        }
        return string;
    }
    
    private String StringParser(){
        String str="";
        while(Json.charAt(index)!='"'){
            str+=Json.charAt(index);
            index++;
        }
        return str;
    } 
    
    private  String subString(char first,char last){
        String str="";
        int number=1;
        int i=index;
        str+=Json.charAt(i);
        i++;
        while(number!=0){
            str+=Json.charAt(i);
            if(Json.charAt(i)==first){number++;}
            if(Json.charAt(i)==last){number--;}
            i++;
        }
        return str;
    }
    
    public <T> T Serializer(String Str,T classofT) {
        this.Json=deleteAdditionalChars(Str);
        Constructor[]constructors=classofT.getClass().getConstructors();
        Class[] classes=constructors[0].getParameterTypes();
        Field[] fields=classofT.getClass().getDeclaredFields();
        for(int i=0;i<fields.length;i++){
            fields[i].setAccessible(true);
            
        }
        String sub;
        int fieldCounter=0;
        for( index=1;index<Json.length()-1;index++){
            index++;
            sub=StringParser();
            index+=2;
            switch(Json.charAt(index)){
              case '"':
              {
                  index++;
                  if(fields[fieldCounter].getName().equals(sub)){
                      try{
                           sub=StringParser();
                           fields[fieldCounter].set(classofT,sub);
                           }catch(IllegalAccessException | IllegalArgumentException ex){}
                           index+=1;
                           fieldCounter++;
                  }
              }
              break;
              case 'n':{
                  if(fields[fieldCounter].getName().equals(sub)){
                      try{
                          fields[fieldCounter].set(classofT,null);
                      }catch(IllegalAccessException | IllegalArgumentException ex){}
                      index+=4;
                      fieldCounter++;
                  }
              }
              break;
              case 't':{
                  if(fields[fieldCounter].getName().equals(sub)){
                      try{
                          fields[fieldCounter].set(classofT,true);
                      }catch(IllegalAccessException | IllegalArgumentException ex){}
                      index+=4;
                      fieldCounter++;
                  }
              }
              break;
              case 'f':{
                  if(fields[fieldCounter].getName().equals(sub)){
                      try{
                          fields[fieldCounter].set(classofT,false);
                      }catch(IllegalAccessException | IllegalArgumentException ex){}
                      index+=5;
                      fieldCounter++;
                  } 
              }
              break;
              case '[':{
                  if (fields[fieldCounter].getName().equals(sub)){
                      try{
                      sub=subString('[', ']');
                      String saveJson=Json;
                      int saveIndex=index+sub.length();
                      index=0;
                      Json=sub;
                      index++;
                      switch(Json.charAt(index)){
                          case '"' :{
                              index++;
                              ArrayList<String> ALtoA=new ArrayList<>();
                              while(Json.charAt(index)!=']'){
                                  ALtoA.add(StringParser());
                                  if(Json.charAt(index+1)==']'){
                                      index++;
                                  }else{
                                      index+=3;
                                  }
                              }
                            String[] toarr=new String[ALtoA.size()];
                            fields[fieldCounter].set(classofT,ALtoA.toArray(toarr));
                          }
                          break;
                          default:{
                              if(doubleOrInt(index)=="int"){
                                  ArrayList<Integer> AltoA=new ArrayList<>();
                                  while(Json.charAt(index)!=']'){
                                      AltoA.add(parseInt());
                                      if(Json.charAt(index)==']'){}else{
                                          index++;
                                      }
                                  }
                                  int[] toarr=new int[AltoA.size()];
                                  for(int i=0;i<AltoA.size();i++){
                                      toarr[i]=AltoA.get(i);
                                  }
                                  fields[fieldCounter].set(classofT, toarr);
                              }
                              
                          }
                      }
                      Json=saveJson;
                      index=saveIndex;
                      /*int i=0;
                      for(;i<classes.length;i++){
                            if(fields[fieldCounter].getType().getName().equals(classes[i].getName())){
                                System.out.println("@@@@@@");
                                break;
                            }    
                      }*/
                      }catch(Exception ex){ex.toString();}
                  }
                 fieldCounter++;
                 index+=0;
              }
              break;
              case '{':{
                  if(fields[fieldCounter].getName().equals(sub)){
                      try{
                          int ind=index+subString('{','}').length();
                          String mstring=Json;
                          int i=0;
                          for(;i<classes.length;i++){
                              if(fields[fieldCounter].getType().getName().equals(classes[i].getName())){
                                  break;
                              }
                              
                          }
                          fields[fieldCounter].set(classofT, Serializer(subString('{','}'), classes[i].newInstance() /*intrue)*/));
                          index=ind;
                          Json=mstring;
                      }catch( IllegalAccessException | IllegalArgumentException | InstantiationException ex){
                          System.out.println(ex.toString());
                      }
                      index+=0;
                      fieldCounter++;
                  } 
              }
              break;
              default :{
                  if(fields[fieldCounter].getName().equals(sub)){
                      try{
                          if(doubleOrInt(index).equals("double")){
                              
                              fields[fieldCounter].set( classofT, parsedouble());
                          }else{
                              fields[fieldCounter].set(classofT, parseInt());
                          }
                      }catch(IllegalAccessException | IllegalArgumentException ex){System.out.println(ex.toString());}
                      fieldCounter++;
                  }
              }
        }
    }
        return classofT;
    }
    
    
}
